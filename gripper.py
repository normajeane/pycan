#!/usr/bin/env python3

import can
import time
import sys


class PyCan:
    def __init__(self, n_node, bit, mode):
        self.bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=bit)
        self.n_node = n_node
        self.mode_of_op = mode

        for i in range(1, self.n_node+1): # 1~n_node
            self.StartRemoteNode(i)
            self.Shutdown(i)
            self.SwitchOn(i)
            self.EnableOperation(i)
            #self.SetTrace(i) # what?

            if self.mode_of_op == 3:
                self.VelocityMode(i)
            elif self.mode_of_op == 1:
                self.PositionMode(i)
            print('----------------------')

    def StartRemoteNode(self, node):
        msg = can.Message(arbitration_id=0x000, data=[0x01, node, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print('Start remote node', node)

    def Shutdown(self, node):
        msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x06, 0x00, 0x00, 0x00], is_extended_id=False)
        self.send_msg(msg)
        print('Shut down', node)
        #self.Shutdown_R(node)

    def Shutdown_R(self, node):
        print('Shut down(Recieve)', node)
        msg = can.Message(arbitration_id=0x580+node, data=[0x60, 0x40, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00], is_extended_id=False)
        try:
            self.bus.send(msg, 0.1)
            print("Message sent on {}".format(self.bus.channel_info))
            recv_msg = self.bus.recv(0.1)
            print(recv_msg)
        except can.CanError:
            print("Message NOT sent")
        time.sleep(0.1)
        print('--')

    def SwitchOn(self, node):
        msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x07, 0x00, 0x00, 0x00], is_extended_id=False)
        self.send_msg(msg)
        print('Switch on', node)

    def EnableOperation(self, node):
        #msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x0E, 0x00, 0x00, 0x00], is_extended_id=False)
        #self.send_msg(msg)

        msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x0F, 0x00, 0x00, 0x00], is_extended_id=False)
        self.send_msg(msg)
        print('Enable', node)

    def DisableOperation(self, node):
        msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x08, 0x00, 0x00, 0x00], is_extended_id=False)
        self.send_msg(msg)
        print('Disable', node)

    def ResetFault(self):
        for i in range(1, self.n_node+1):
            self.DisableOperation(i)
            self.SwitchOn(i)
            self.EnableOperation(i)

    def SetTrace(self, node):
        msg = can.Message(arbitration_id=0x400+node, data=[0xC8, 0x04, 0x01, 0x01, 0x01], is_extended_id=False)
        self.send_msg(msg)

    def DisableVoltage(self, node):
        msg = can.Message(arbitration_id=0x600+node, data=[0x2B, 0x40, 0x60, 0x00, 0x0D, 0x00, 0x00, 0x00], is_extended_id=False)
        self.send_msg(msg)

    def grip_setAngle(self, node, ang, max_speed):
        self.TargetPosition(node, self.D2P(ang))
        self.LoadMaxSpeed(node, max_speed)
        self.MoveAbsoluteToNewPoint(node)

    def MoveAbsoluteToNewPoint(self, node): # msg -> msg_manp
        msg = can.Message(arbitration_id=0x200+node,data=[0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print('Move the target position', node)

    def LoadMaxSpeed(self, node, maxVelocity): # msg -> msg_lms
        maxVelo1 = maxVelocity & 0xFF
        maxVelo2 = (maxVelocity >> 8) & 0xFF
        maxVelo3 = (maxVelocity >> 16) & 0xFF
        maxVelo4 = (maxVelocity >> 24) & 0xFF
        msg = can.Message(arbitration_id=0x600+node,data=[0x23, 0x7F, 0x60, 0x00, maxVelo1, maxVelo2, maxVelo3, maxVelo4],is_extended_id=False)
        self.send_msg(msg)

    def D2P(self, deg):
        return int(20 * 30 * 4096 / 360 * deg)

    def VelocityMode(self, node): # msg -> msg_vm
        msg = can.Message(arbitration_id=0x600+node,data=[0x2F, 0x60, 0x60, 0x00, 0x03, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print(node, 'th node was set the profile velocity mode.')

    def PositionMode(self, node): # msg -> msg_pm
        msg = can.Message(arbitration_id=0x600+node,data=[0x2F, 0x60, 0x60, 0x00, 0x01, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print(node, 'th node was set the profile position mode.')

    def HomingMode(self, node): # msg -> msg_pm
        msg = can.Message(arbitration_id=0x600+node,data=[0x2F, 0x60, 0x60, 0x00, 0x06, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print(node, 'th node was set the homing mode.')

    def TargetVelocity(self, node, velocity): # msg -> msg_tv
        vel1 = velocity & 0xFF
        vel2 = (velocity >> 8) & 0xFF
        vel3 = (velocity >> 16) & 0xFF
        vel4 = (velocity >> 24) & 0xFF
        msg = can.Message(arbitration_id=0x600+node,data=[0x23, 0xFF, 0x60, 0x00, vel1, vel2, vel3, vel4],is_extended_id=False)
        self.send_msg(msg)
        print('Target velocity:', velocity)
        print('Target velocity:', vel1, vel2, vel3, vel4)

    def TargetPosition(self, node, position):
        pos1 = position & 0xFF
        pos2 = (position >> 8) & 0xFF
        pos3 = (position >> 16) & 0xFF
        pos4 = (position >> 24) & 0xFF
        msg = can.Message(arbitration_id=0x600+node,data=[0x23, 0x7A, 0x60, 0x00, pos1, pos2, pos3, pos4],is_extended_id=False)
        self.send_msg(msg)
        print('Target position:', position)

    # TODO
    def Homeing(self, node, velocity): # msg -> msg_tv
        vel1 = velocity & 0xFF
        vel2 = (velocity >> 8) & 0xFF
        vel3 = (velocity >> 16) & 0xFF
        vel4 = (velocity >> 24) & 0xFF
        #6098

        msg = can.Message(arbitration_id=0x600+node,data=[0x23, 0xFF, 0x60, 0x00, vel1, vel2, vel3, vel4],is_extended_id=False)
        self.send_msg(msg)

    def ResetPosition(self, node): # msg -> msg_rp
        msg = can.Message(arbitration_id=0x300+node,data=[0xB8, 0x00, 0x00, 0x00, 0x00],is_extended_id=False)
        self.send_msg(msg)
        print('Reset position')

    def OperationDisable(self):
        for i in range(1, self.n_node+1):
            self.DisableVoltage(i)
            self.Shutdown(i)

    def OperationEnable(self):
        for i in range(1, self.n_node+1):
            self.SwitchOn(i)
            self.EnableOperation(i)

    def grip_init_pose(self):
        for i in range(1, self.n_node+1):
            self.PositionMode(i)

        self.grip_setAngle(1, -32, 3000)
        self.grip_setAngle(2, -49, 1000)
        self.grip_setAngle(3, -49, 1000)

    def grip_open(self):
        for i in range(1, self.n_node+1):
            self.PositionMode(i)

        self.grip_setAngle(1, -32, 5500)
        self.grip_setAngle(2, -49, 5500)
        self.grip_setAngle(3, -49, 5500)

    def grip_close(self):   # only joint 1
        self.VelocityMode(1)
        self.TargetVelocity(1, -3000)

    def grip_vel_stop(self):
        for i in range(1, self.n_node+1):
            self.VelocityMode(i)
            self.TargetVelocity(i, 0)

    def grip_pos_stop(self):
        for i in range(1, self.n_node+1):
            self.PositionMode(i)
            print('TODO')

    def send_msg(self, msg):
        try:
            self.bus.send(msg, 0.1)
            print("Message sent on {}".format(self.bus.channel_info))
            recv_msg = self.bus.recv(0.1)
            print(recv_msg)
        except can.CanError:
            print("Message NOT sent")
        time.sleep(0.1)
        print('--')


if __name__ == '__main__':

    n_node = 3
    bitrate = 125000
    #bitrate = 1000000
    mode = 3            # 3 : profile velocity mode

    pygrip = PyCan(n_node, bitrate, mode)
    #pygrip.TargetVelocity(1, -500)
    key = input()
    if key == 'q':
        sys.exit()

#    pygrip.ResetFault() # Not sent.. why?
#    pygrip.grip_init()
#    pygrip.grip_open()
#    pygrip.grip_close()
