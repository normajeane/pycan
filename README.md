# python-can

The python-can library provides Controller Area Network support for Python, providing common abstractions to different hardware devices, and a suite of utilities for sending and receiving messages on a CAN bus.

python-can runs any where Python runs; from high powered computers with commercial CAN to usb devices right down to low powered devices running linux such as a BeagleBone or RaspberryPi.


# SocketCAN
**The socketcan package is an implementation of CAN protocols (Controller Area Network) for Linux.**

While there have been other CAN implementations for Linux based on character devices, **SocketCAN uses the Berkeley socket API**, the Linux network stack and implements the CAN device drivers as network interfaces.  

The CAN socket API has been designed **as similar as possible to the TCP/IP protocols** to allow programmers, familiar with network programming, to easily learn how to use CAN sockets.

 (for example) to enable an existing can0 interface with a bitrate of 1MB:

  `sudo ip link set can0 up type can bitrate 1000000`

 (for example) CAN Error

  `ip link set can0 type can restart`


 (for example) Transmit a message to the CAN bus.

 `send(msg, timeout=None)`

 - timeout (float): Wait up to this many seconds for the transmit queue to be ready. If not given, the call may fail immediately.


# IXXAT USB-to-CAN V2

```
make all

sudo make install
```

## Permission error

* Reason : Secure boot

* Solution[^1]
```
sudo apt install mokutil

sudo mokutil --disable-validation

reboot

Change Secure Boot state
```

# Mode of Operation (0x6060)

  Profile position mode: 1
  Profile velocity mode: 3
  Homing mode:            6


[^1]: https://wiki.ubuntu.com/UEFI/SecureBoot/DKMS
